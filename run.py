import argparse

from lib.PySettings import SettingsManager
from project.entity.dao import init_db
from project.importer.Importer import *
from project.simulator import Simulator
from datetime import datetime


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Tool to simulate Atac in Rome.')
    parser.add_argument('-settings', help='Show local configuration', required=False, default=False, action="store_true")
    parser.add_argument('-init', help='Create a local sqlite db', required=False, default=False, action="store_true")
    parser.add_argument('-simulator', help='Start simulator', required=False,default=False, action="store_true")
    parser.add_argument('-importer', help='Start importer', required=False,default=False, action="store_true")
    parser.add_argument('-log', help='Start log', required=False,default=False, action="store_true")
    parser.add_argument('-daemon', help='Start log', required=False,default=False, action="store_true")
    opts = parser.parse_args()

    if opts.settings:
        print str(SettingsManager.getSettings())
    if opts.init:
        print "Initializing db sqlite"
        init_db()
        print "Initialized"
    if opts.simulator:
        print "Loading simulator"

        #simulator = Simulator(datetime(2017,7,21,2,9,12), 0.4)
        #simulator.send_bus(datetime(2017,7,19,16,30,0),504,74769,504)
        #print "Done"

        Simulator.start()
    if opts.importer:
        print "Loading importer"
        search_stops_by_settings()
        print "Done"
    if opts.log:
        print "Loading logger"
        save_position_all_net()
        print "Done"
    if opts.daemon:
        import sys,datetime
        sys.stdout = open(str(datetime.date.today()) + ".log", 'w')
        print "Loading logger daemon"
        daemon_logger()
        print "Done"
