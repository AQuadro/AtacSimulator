SELECT id,station_id as station, instant as instant_dep, bus_id as bus, line_id as line
FROM logevent
group by instant
HAVING  0 = (
	Select count(*)
    FROM logevent
    WHERE instant < Datetime(instant_dep, '+10 minutes')
    AND instant > instant_dep
    AND bus_id = bus
    AND station_id=station
    AND line_id = line
) 
AND 0 < (
	Select count(*)
    FROM logevent
    WHERE instant < Datetime(instant_dep, '+1000 minutes')
    AND instant > instant_dep
    AND bus_id = bus
    AND station_id=station
    AND line_id = line
)