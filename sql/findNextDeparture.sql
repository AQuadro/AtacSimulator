SELECT id,station_id, instant as instant_dep, bus_id as bus, line_id as line
FROM logevent
WHERE station_id=74769
AND line_id=504
AND instant > Datetime("2017-07-19 16:30:00" )
group by instant
HAVING  0 = (
	Select count(*)
    FROM logevent
    WHERE instant < Datetime(instant_dep, '+10 minutes')
    AND instant > instant_dep
    AND bus_id = bus
    AND station_id=74769
    AND line_id = line
)
order by instant ASC
limit 1
