#!/usr/bin/env bash
wget https://bootstrap.pypa.io/get-pip.py -q -O get-pip.py
sudo python get-pip.py
rm get-pip.py
sudo pip install sqlalchemy==1.1.11
cd lib
rm -rf PySettings
git clone https://github.com/AQuadroTeam/PySettings.git

