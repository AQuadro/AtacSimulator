.. project documentation master file, created by
   sphinx-quickstart on Fri Jul 21 18:46:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to project's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   project


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
