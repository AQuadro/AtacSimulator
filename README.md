# Atac Simulator
Questo progetto ha come scopo la realizzazione di un ambiente di test per controllori di capolinea Atac a Roma.

## Documentazione Codice Sorgente
https://aquadro.gitlab.io/AtacSimulator/

## Autori
- Alessandro Fazio
- Andrea Gennusa (andrea.gennusa@gmail.com)

## Assunzioni sulla rete
Per semplicità si è preso in considerazione l'operato di un singolo capolinea, per valutarne gli effetti sulle fermate servite dalla stessa.
Sono state considerate sia linee "chiuse" che partono e arrivano allo stesso capolinea, sia linee che hanno una linea corrispettiva di ritorno verso il capolinea opposto.

## Componenti funzionali

E' composto da 3 tipi di componenti:
- Simulatore di rete
- Metricatore della qualità di servizio
- Controllore di capolinea

Cliccando sul titolo si accede alla documentazione.
### [1. Simulatore](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.Simulator)
Il simulatore della rete di autobus Atac costituisce il cuore del progetto. Deve fornire una sandbox credibile in cui testare controllori di capolinea.

La funzionalità principale che deve offrire è costituita dalla stima della durata del percorso di un autobus con partenza decisa dal controllore in base a monitoraggi di giorni precedenti della rete di Roma.

Inoltre deve fornire una stima basata sui dati a disposizione del controllore del tempo di ritorno dello stesso autobus, costituendo una previsione per il controllore.

#### Reperimento dei dati
Utilizzando le Api pubbliche messe a disposizione da www.muoversiaroma.it abbiamo scelto un capolinea di riferimento (nel nostro caso Anagnina) ed abbiamo selezionato tutte le linee dello stesso.
Si è costruito un importatore che tramite polling scansiona lo stato delle linee prese in esame salvando la posizione degli autobus presenti in esse.
Tale insieme di tuple autobus-posizione-orario rappresentano la base di dati da cui inferire stime.

#### Inferenza sui dati
Per fornire previsioni e stime il controllore si basa su dati di giorni precedenti reperibili dalle Api pubbliche di Muoversi A Roma.
Il simulatore, data un'ora di partenza di una corsa simulata, valuta la durata di una corsa di caratteristiche analoghe con partenza precedente alla simulata. Interpolando corse reali immediatamente precedenti e successive (eventualmente di grado variabile) si ottiene una stima verosimile della durata della corsa.


### [2. Metricatore](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.Metricator)
Il simulatore permette di registrare Metricatori che hanno come scopo la cattura della qualità dell'operato di un controllore in base ad una metrica.

#### [Metricatore di Regolarità](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.RegularityMetricator)
Data l'assenza di una vera e propria timetable per i servizi offerti da Atac è stato sviluppato un metricatore in grado di misurare la regolarità di partenze di autobus offerte su ogni tratta.
Requisito importante infatti per linee a frequenza è proprio quello di erogare con continuità il servizio, mirando ad una frequenza stabile di invio degli autobus durante la giornata.

Il controllore infatti dovrebbe pianificare le successive partenze in modo da tener conto degli autobus realmente a disposizione per evitare di avere momenti della giornata in cui non è in grado di garantire la regolarità di autobus sulle linee.

Il Metricatore di regolarità restituisce per ogni linea la media di intervalli fra partenze e la varianza espressa in minuti.

### [3. Controllore](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.StationController)
Il controllore di capolinea è l'elemento funzionale che ha la responsabilità della scelta dell'orario di partenza degli autobus sul capolinea scelto come riferimento.
Il controllore immediatamente disponibile dai log catturati è proprio l'operato dell'Atac.
Eventuali altri controllori hanno come dati a disposizione solo le stime offerte dal simulatore, ma non il vero orario di ritorno di ogni autobus.

#### [Controllore Atac](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.AtacStationController)
Per offrire una linea base per il confronto dell'operato di qualsiasi altro controllore è stato realizzato il controllore Atac.
Questo controllore, ricavando dagli eventi di log gli orari delle reali partenze emula il comportamento del capolinea scelto nella simulazione.

##### Corse Simulate
L'orario delle partenze di ogni autobus è ricavato direttamente dai log. Una volta estratta l'informazione sugli orari di partenza viene simulato l'invio dell'autobus nel simulatore, che stimerà secondo i dati reali l'orario di ritorno.

##### Buffer Autobus
Per rendere più completa la simulazione si è considerato anche un tempo configurabile di attesa fra l'arrivo di un bus in stazione e la sua prima ripartenza possibile. Questo fattore, insieme alla presenza di un buffer limitato per la stazione dal quale attingere autobus da inviare, pone vincoli alla creazione di nuove corse.

Si è creato un sistema a doppia coda, in cui gli autobus in arrivo vengono posti nel buffer di attesa per un tempo prefissato, per poi essere trasferiti alla fine di esso nel buffer di bus disponibili.

Nel momento di creazione di una nuova corsa viene estratto un bus dalla lista di disponibilità, e nel momento in cui non ce ne sia nessuno disponibile viene creato un evento in coda che il controllore potrà risolvere in futuro non appena un autobus tornerà disponibile nella coda di disponibilità.

## Tecnologia
Per velocità di sviluppo e data l'assenza di requisiti non funzionali stringenti nel campo come linguaggio principale di programmazione si è scelto Python 2.7.
La struttura del programma è integrata con un modulo per la gestione delle impostazioni PySettings (sviluppato da noi https://github.com/AQuadroTeam/PySettings).

Per relazionarsi alla base di dati si è scelta la libreria SqlAlchemy.

## Installazione e Avvio
### Installazione
In ambiente Linux è necessario disporre di APT installato. Per installare le dipendenze avviare:
```
./setup.sh
```
### Manuale importazione dati
Il programma si presenta all'utente tramite il file "run.py". Eseguire in ordine i seguenti passi per avviare correttamente il programma.
#### Help rapido
```
python run.py -h
```
#### Inizializzazione del DB
```
python run.py -init
```
#### Importazione linee e fermate
```
python run.py -import
```
#### Avvio del demone di monitoraggio autobus
```
python run.py -daemon &
```
Il log verrà salvato in un file chiamato con la data attuale.

### Manuale simulazione
Viene fornito un database contenente log di alcuni giorni "bigAtacSimulatordb", preimpostato nel file di impostazioni.
Per avviare la simulazione è necessario eseguire:
```
python run.py -simulator
```
## Come preparare una simulazione
```
    begin_simulation = SettingsManager.getSettings().get_begin_simulation()
    end_simulation = SettingsManager.getSettings().get_end_simulation()
    sampling_rate_seconds = SettingsManager.getSettings().get_sampling_rate()
    station_name = SettingsManager.getSettings().get_station_name()
    sampling_rate = timedelta(seconds=sampling_rate_seconds)
    pause_duration = timedelta(seconds=SettingsManager.getSettings().get_pause_duration())
    bus_buffer = SettingsManager.getSettings().get_bus_buffer()
```
Per prima cosa è necessario caricare i parametri necessari alla configurazione di una simulazione. Nell'esempio di codice estratto da
[Simulator.start()](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html?highlight=start#project.simulator.Simulator.start) 
viene mostrato come prendere questi parametri dal file di configurazione. 

```
    simulator = Simulator(begin_simulation, end_simulation, sampling_rate)
```
Tramite questi parametri è possibile istanziare un simulatore chiamando il [costruttore della classe Simulator](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.Simulator.Simulator.__init__).
```
    simulator.add_controller(AtacStationController(sampling_rate, station_name, pause_duration, simulator, bus_buffer))
    simulator.add_metricator(RegularityMetricator())
```
Adesso possono essere aggiunti al simulatore i metricatori e i controllori.
```
    simulator.loop()
```
Ed infine la simulazione può essere avviata con la funzione [loop](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.Simulator.Simulator.loop). 

## Come creare un Controllore
L'interfaccia [StationController](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.StationController) richiede solamente un metodo pubblico [iteration](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.StationController.StationController.iteration).
Tale metodo viene chiamato ad ogni iterazione della simulazione. I parametri di  [iteration](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.StationController.StationController.iteration) 
sono l'istante attuale della simulazione e l'elenco di eventi scaduti fra la scorsa iterazione e l'attuale. 

I metodi pubblici di [Simulator](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#module-project.simulator.Simulator) utilizzabili dalla StationController sono solamente
[send_bus](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.Simulator.Simulator.send_bus) e 
[eta](https://aquadro.gitlab.io/AtacSimulator/docs/project.simulator.html#project.simulator.Simulator.Simulator.eta), 
che rispettivamente forniscono la funzionalità di invio di una corsa simulata (il simulatore genererà un EventOfArrival futuro) e la stima del ritorno di una determinata corsa (presente o meno nel simulatore).

La StationController dovrà provvedere internamente alla gestione delle pause degli autisti e la gestione interna degli autobus disponibili.   

## Impostazioni
- db_name : bigAtacSimulatordb -> il path relativo del database da utilizzare
- db_engine : sqlite -> il tipo di database con cui il programma dovrà interagire
- station_name : anagnina -> il nome del capolinea da prendere in esame, le scelte più significative sono "anagnina" e "termini". Il programma cercherà automaticamente le "paline" corrispondenti al capolinea.
- base_url_muoversiaroma : http://muovi.roma.it/json/ -> path delle API offerte da MuoversiARoma
- threads : 10 -> numero di thread concorrenti utilizzati per interrogare le API di MuoversiARoma durante il polling per salvare i log sulla posizione dei mezzi.
- interval_second_log : 60 -> intervallo di tempo in secondi fra una interrogazione e l'altra del demone per importare i log sulla posizione degli autobus.
- threshold_min : 600 -> durata minima di una corsa in secondi. Dato che ogni autobus registra diversi log in ogni stazione, il sistema utilizza un threshold minimo per differenziare da eventi di autobus in pausa e eventi di corsa.
- threshold_max : 21600 -> durata massima di una corsa in secondi, utilizzata dal sistema per identificare situazioni in cui l'autobus non è più in servizio per molte ore. Ad esempio un autobus che finisce il servizio alle 20 e poi torna disponibile alle 8 del giorno dopo.
- begin_simulation : 2017-7-19/00:00:00 -> orario di inizio simulazione
- end_simulation : 2017-7-20/00:00:00 ->  orario di fine simulazione
- sampling_rate : 60 -> durata di ogni step della simulazione in secondi
- pause_duration : 600 -> Durata in secondi della pausa fra un arrivo in capolinea e la successiva partenza per gli autobus
- verbosity : 9 -> Livello di verbosità di output
- bus_buffer : 5 -> Numero di autobus disponibile per il capolinea.
