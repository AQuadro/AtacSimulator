#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from Base import Base

class LogEvent(Base):
    __tablename__ = "logevent"

    id = Column(Integer, primary_key=True, autoincrement=True)
    station_id = Column(Integer, ForeignKey("station.id"))
    instant = Column(DateTime, primary_key=False)
    bus_id = Column(Integer, ForeignKey("autobus.id"))
    line_id = Column(String, ForeignKey("line.id"))

    station = relationship("Station")
    bus = relationship("Autobus")
    line = relationship("Line")

    '''
    ULTIMA BUONA SENZA due attributi
    lines = relationship("Line",back_populates="station", foreign_keys=[line_id])
    '''
