'''
from Base import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


class Association(Base):
    __tablename__ = 'association'
    station_id = Column(Integer, ForeignKey('left.id'), primary_key=True)
    line_id = Column(Integer, ForeignKey('right.id'), primary_key=True)
    extra_data = Column(String(50))
    line = relationship("Line", back_populates="stations")
    station = relationship("Station", back_populates="lines")
'''
