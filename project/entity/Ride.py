from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from Base import Base

class Ride(Base):
    __tablename__ = "ride"

    id = Column(Integer, primary_key=True, autoincrement=True)
    line_id = Column(String, ForeignKey("line.id"))
    autobus_id = Column(Integer, ForeignKey("autobus.id"))
    departure_time = Column(DateTime)
    arrival_time = Column(DateTime)
    departure_station = Column(Integer)

    autobus = relationship("Autobus")
    line = relationship("Line")


    def __str__(self):
        return "Ride id {}, on line {}, departure {}, arrival {}".format(self.id, self.line_id, self.departure_time, self.arrival_time)