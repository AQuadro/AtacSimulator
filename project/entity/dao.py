#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from Base import Base
from Line import Line
from Station import Station
from Ride import Ride
from Autobus import Autobus
from lib.PySettings import SettingsManager
from datetime import datetime
from EventOfArrival import EventOfArrival
from LogEvent import LogEvent
from sqlalchemy import text

FIRST = "first"
ALL = "all"


def get_engine():
    settings = SettingsManager.getSettings()
    db_name = settings.get_dbname()
    db_engine = settings.get_db_engine()

    # to build database, execute this file

    #Not secure!!! warning injection!
    engine = create_engine(db_engine + ':///' + db_name)

    return engine

def add_line(line):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    ses.add(line)
    ses.commit()


def add_station(station):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    ses.add(station)
    ses.commit()


def get_station_by_id(id):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    Session = sessionmaker(bind=eng)
    ses = Session()
    return ses.query(Station).filter(Station.id.like(id))


def populate(stations=[], lines=[], autobus=[], rides=[], events_of_arrival=[], log_events=[]):
    """
        Populate db with every instance (if exists)
        :param query: stations
        :type query: list
        :param query: lines
        :type query: list
        :param query: autobus
        :type query: list
        :param query: rides
        :type query: list
        :return: returns nothing
        :rtype: None
    """
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    for s in stations:
        ses.merge(s)
    for l in lines:
        ses.merge(l)
    for a in autobus:
        ses.merge(a)
    for r in rides:
        ses.merge(r)
    for e in events_of_arrival:
        ses.merge(e)
    for v in log_events:
        ses.merge(v)

    ses.commit()

def query_wrapper(query, post_proc=ALL, limit=0):
    """
        Wrapper to post process something with the returned query
        :param query: line id
        :type query: Query
        :param post_proc: everything supported by query methods, e.g. first result, all ...
        :type post_proc: string
        :param query: limit to returned results, must be greater than 0
        :type query: int
        :return: returns an Object
        :rtype: Object
    """
    qq = None
    if limit > 0:
        qq = query.limit(limit)
        query = qq

    try:
        return getattr(query, post_proc)()
    except AttributeError:
        print "You entered a method that is not recognized by sqlalchemy"
        return


def find_all_monitor():
    return find(all_monitor,[],ALL,0)


def all_monitor(ses):
    query = (ses.query(Station)
             .filter_by(Station.monitor is 1)
             )
    return query


def find_first_arrival_time_by_line(line):
    """
        :param line: line to search
        :type line: Line
        :return: returns first arrival time
        :rtype: DateTime
    """
    #roba inutile tenuta per ricordarsi la conversione anche se non serve farla
    #ricordiamo che quando si stampa un datetime lo stampa già formattato a stringa
    #datetime_object = datetime.strptime('2017-07-14 02:09:12', '%b %d %Y %I:%M%p')
    #return datetime.strptime(ff, '%Y-%m-%d %H:%M:%S')

    ff =  find_arrival_ride_by_line_id(line.id, FIRST)
    if ff is not None:
        return ff.arrival_time
    else: return None

def find_arrival_ride_by_line_id(line_id, post_proc=ALL, limit=0):
    return find(arrival_ride_by_line_id,[line_id],post_proc,limit)

def arrival_ride_by_line_id(line_id,ses):
    """
        Returns one or more arrival rides by line id
        :param id: line id
        :type id: int
        :return: returns first arrival ride
        :rtype: Ride
    """
    query = (ses.query(Ride)
            .join(Line, Line.id == Ride.line_id)
            .filter(Ride.line_id == line_id)
            .order_by(Ride.arrival_time)
            )
    return query


def find_line_by_id(id,post_proc=ALL,limit=0):
    return find(line_by_id,[id],post_proc,limit)


def find_stations_by_name(name,post_proc=ALL,limit=0):
    return find(stations_by_name,[name],post_proc,limit)

def stations_by_name(name, ses):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    Session = sessionmaker(bind=eng)
    ses = Session()

    return ses.query(Station).filter(Station.name.like(name))


def line_by_id(id, ses):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    Session = sessionmaker(bind=eng)
    ses = Session()

    return ses.query(Line).filter_by(Line.id.like(id))

def find_all_lines():
    return find(all_lines,[],ALL,0)

def all_lines(ses):
    return  ses.query(Line)

def find_log_event_by_bus_id_and_station_id(bus_id, station_id,post_proc=ALL,limit=0):
    return find(log_event_by_bus_id_and_station_id,[bus_id,station_id],post_proc,limit)

def log_event_by_bus_id_and_station_id(bus_id, station_id, ses):
    query = (ses.query(LogEvent)
            .filter(LogEvent.bus_id == bus_id , LogEvent.station_id == station_id)
            .order_by(LogEvent.instant)
            )

    return query

def find_arrival_by_bus_id_line_id_station_id(instant, bus_id, line_id, station_id, prev=True):
    return find(arrival_by_bus_id_line_id_station_id,[instant, bus_id, line_id, station_id, prev],FIRST,0)

def arrival_by_bus_id_line_id_station_id(instant, bus_id, line_id, station_id, prev, ses):
    # prendi orario di autobus x prima di orario y su linea z e stazione v
    if prev:
        query = (ses.query(LogEvent)
                .join(Line, LogEvent.station_id == Line.departure_station_id)
                .filter(LogEvent.bus_id == bus_id , LogEvent.station_id == station_id ,
                 LogEvent.instant <= instant, Line.id == line_id)
                .order_by(LogEvent.instant)
                )
    else:
        query = (ses.query(LogEvent)
                .join(Line, LogEvent.station_id == Line.departure_station_id)
                .filter(LogEvent.bus_id == bus_id , LogEvent.station_id == station_id ,
                 LogEvent.instant >= instant, Line.id == line_id)
                .order_by(LogEvent.instant)
                )
    return query

def find_prev_filtered_arrival(instant, line_id, station_id, bus_id):
    # prendi orario di autobus x prima di orario y su linea z e stazione v
    settings = SettingsManager.getSettings()
    min_thr = settings.get_threshold_min()
    max_thr = settings.get_threshold_max()
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    plus_min_thr = "'+{} minutes'".format(str(min_thr/60))
    #plus = '+{} minutes'
    print "instant {}, station_id {}, line_id {}, plus_min_thr {}".format(instant, station_id, line_id, plus_min_thr)

    query = text("SELECT id,station_id, instant as instant_dep, bus_id as bus, line_id as line\
        FROM logevent\
        WHERE station_id={}\
        AND line_id='{}'\
        AND bus_id = {}\
        AND instant > '{}'\
        group by instant\
        order by instant ASC\
        limit 1".format(station_id,line_id,bus_id,instant))
    #simple_query = "SELECT * FROM logevent"
    #print "####### QUERY ######\n{}".format(query)
    return eng.execute(query).first()

def find_atac_departures(left_bound, right_bound):
    settings = SettingsManager.getSettings()
    min_thr = settings.get_threshold_min()
    max_thr = settings.get_threshold_max()
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    plus_min_thr = "'+{} minutes'".format(str(min_thr/60))
    plus_max_thr = "'+{} minutes'".format(str(max_thr/60))

    query = text("\
        SELECT id,station_id as station, instant as instant_dep, bus_id as bus, line_id as line \
        FROM logevent \
        WHERE instant >= Datetime('{}') \
        AND instant <= Datetime('{}')   \
        GROUP BY instant \
        HAVING  0 = ( \
            SELECT COUNT(*) \
            FROM logevent \
            WHERE instant < Datetime(instant_dep, {}) \
            AND instant > instant_dep \
            AND bus_id = bus \
            AND station_id=station \
            AND line_id = line \
        ) \
        AND 0 < ( \
            SELECT COUNT(*) \
            FROM logevent \
            WHERE instant < Datetime(instant_dep, {}) \
            AND instant > instant_dep \
            AND bus_id = bus \
            AND station_id=station \
            AND line_id = line \
    )".format(left_bound, right_bound, plus_min_thr, plus_max_thr))

    if SettingsManager.getSettings().get_verbosity() > 9: print "####### QUERY ######\n{}",format(query)
    return eng.execute(query).fetchall()

def find_next_filtered_arrival(instant, line_id, station_id, bus_id):
    # prendi orario di autobus x prima di orario y su linea z e stazione v
    settings = SettingsManager.getSettings()
    min_thr = settings.get_threshold_min()
    max_thr = settings.get_threshold_max()
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    plus_min_thr = "'+{} minutes'".format(str(min_thr/60))
    #plus = '+{} minutes'
    print "instant {}, station_id {}, line_id {}, plus_min_thr {}".format(instant, station_id, line_id, plus_min_thr)

    query = text("SELECT id,station_id, instant as instant_dep, bus_id as bus, line_id as line\
        FROM logevent\
        WHERE station_id={}\
        AND line_id='{}'\
        AND bus_id = {}\
        AND instant > '{}'\
        group by instant\
        order by instant ASC\
        limit 1".format(station_id,line_id,bus_id,instant))
    #simple_query = "SELECT * FROM logevent"
    #print "####### QUERY ######\n{}".format(query)
    return eng.execute(query).first()


def find_prev_filtered_departure(instant, line_id, station_id):
    # prendi orario di autobus x prima di orario y su linea z e stazione v
    settings = SettingsManager.getSettings()
    min_thr = settings.get_threshold_min()
    max_thr = settings.get_threshold_max()
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    plus_min_thr = "'+{} minutes'".format(str(min_thr/60))
    #plus = '+{} minutes'
    print "instant {}, station_id {}, line_id {}, plus_min_thr {}".format(instant, station_id, line_id, plus_min_thr)

    query = text("SELECT id,station_id as station, max(instant) as instant_dep, bus_id as bus, line_id as line\
        FROM logevent\
        WHERE station_id={}\
        AND line_id='{}'\
        AND instant < '{}'\
        group by instant\
        HAVING  0 = (\
        	Select count(*)\
            FROM logevent\
            WHERE instant < Datetime(instant_dep, {})\
            AND instant > instant_dep\
            AND bus_id = bus\
            AND station_id=station\
            AND line_id = line\
        )\
        ORDER BY instant DESC\
        LIMIT 1".format(station_id, line_id, instant, plus_min_thr))
    #simple_query = "SELECT * FROM logevent"
    #print "####### QUERY ######\n{}".format(query)
    return eng.execute(query).first()


def find_next_filtered_departure(instant, line_id, station_id):
    # prendi orario di autobus x prima di orario y su linea z e stazione v
    settings = SettingsManager.getSettings()
    min_thr = settings.get_threshold_min()
    max_thr = settings.get_threshold_max()
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    plus_min_thr = "'+{} minutes'".format(str(min_thr/60))
    #plus = '+{} minutes'
    print "instant {}, station_id {}, line_id {}, plus_min_thr {}".format(instant, station_id, line_id, plus_min_thr)

    query = text("SELECT id,station_id as station, instant as instant_dep, bus_id as bus, line_id as line\
            FROM logevent\
            WHERE station_id={}\
            AND line_id='{}'\
            AND instant > '{}'\
            group by instant\
            HAVING  0 = (\
            	Select count(*)\
                FROM logevent\
                WHERE instant < Datetime(instant_dep, {})\
                AND instant > instant_dep\
                AND bus_id = bus\
                AND station_id=station\
                AND line_id = line)\
            order by instant ASC\
            limit 1".format(station_id, line_id, instant, plus_min_thr))
    #simple_query = "SELECT * FROM logevent"
    #print "####### QUERY ######\n{}".format(query)
    return eng.execute(query).first()

def find(method,args, post_proc,limit):
    """
        Wrapper to find something, just enter a function pointer
    """
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    inputs = args
    inputs.append(ses)
    return query_wrapper(method(*inputs), post_proc, limit)

def init_db():
    # to build database, execute this file
    engine = get_engine()
    Base.metadata.create_all(engine, checkfirst=True)

    s1 = Station(id=1)
    s2 = Station(id=2)

    l1 = Line(departure_station=s1,middle_station=s2,id="333")
    s1.departure_lines.append(l1)
    s2.middle_lines.append(l1)

    a1 = Autobus(id=1)
    a2 = Autobus(id=2)

    r1 = Ride(line=l1, autobus=a1, departure_time=func.now(), arrival_time=datetime(2017,7,14,2,9,12))
    r2 = Ride(line=l1, autobus=a2, departure_time=func.now(), arrival_time=datetime(2017,7,14,2,9,13))

    e1 = LogEvent(station_id=s1.id, bus_id=a1.id, instant=func.now(), line_id=l1.id)
    e2 = LogEvent(station_id=s2.id, bus_id=a2.id, instant=func.now(), line_id=l1.id)

    rr1 = EventOfArrival(ride=r1, creation=func.now(), expiration=func.now())
    rr2 = EventOfArrival(ride=r2, creation=func.now(), expiration=func.now())

    #populate(stations=[s1,s2],lines=[l1],autobus=[a1,a2],rides=[r1,r2], log_events=[e1,e2], events_of_arrival=[rr1,rr2])

    #fake line
    l2 = Line(departure_station=s2,middle_station=s1,id="21987213")

    #first_arrival = find_arrival_ride_by_line_id(line_id=l1.id, post_proc=FIRST, limit=5)
    #first_arrival = find_first_arrival_time_by_line(l1)
    #print "First arrival at : {}".format(first_arrival)

    #first_log_event = find_log_event_by_bus_id_and_station_id(a1.id,s1.id,post_proc=FIRST)
    #print "First log event at : {}".format(first_log_event.instant)

    #prev_arrival = find_arrival_by_bus_id_line_id_station_id(func.now(), a1.id, l1.id, s1.id, False)
    #print "Prev arrival at : {}".format(prev_arrival.instant)
