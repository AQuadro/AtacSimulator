#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from Base import Base

class Station(Base):
    __tablename__ = "station"

    id = Column(Integer, primary_key=True)
    direction = Column(String, primary_key=False)
    name = Column(String, primary_key=False)
    monitor = Column(Integer)

    '''
    ULTIMA BUONA SENZA due attributi
    lines = relationship("Line",back_populates="station", foreign_keys=[line_id])
    '''
    def __str__(self):
        return "Station of {}, id {}, direction {}".format(self.name, self.id, self.direction)