#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from Base import Base

class Line(Base):

    __tablename__ = "line"

    id = Column(String, primary_key=True)

    #station_id = Column(Integer, ForeignKey("station.id"))

    departure_line_id = Column(String)
    arrival_line_id = Column(String)
    departure_station_id = Column(Integer, ForeignKey("station.id"))
    middle_station_id = Column(Integer, ForeignKey("station.id"))
    loop_line = Column(Integer)

    departure_station = relationship("Station", backref="departure_lines", foreign_keys=[departure_station_id])
    middle_station = relationship("Station", backref="middle_lines", foreign_keys=[middle_station_id])
