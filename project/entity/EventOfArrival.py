#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from Base import Base


class EventOfArrival(Base):
    __tablename__ = "eventofarrival"

    id = Column(Integer, primary_key=True, autoincrement=True)
    ride_id = Column(Integer, ForeignKey("ride.id"))
    creation = Column(DateTime, primary_key=False)
    expiration = Column(DateTime, primary_key=False)

    ride = relationship("Ride")
    '''
    ULTIMA BUONA SENZA due attributi
    lines = relationship("Line",back_populates="station", foreign_keys=[line_id])
    '''

    def __cmp__(self, other):
        return self.expiration > other.expiration