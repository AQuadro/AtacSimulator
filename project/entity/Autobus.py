from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from Base import Base

class Autobus(Base):

    __tablename__ = "autobus"

    id = Column(Integer, primary_key=True)
