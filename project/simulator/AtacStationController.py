from project.entity.dao import *
from lib.PySettings.SettingsManager import SettingsManager
import datetime
from StationController import StationController
import heapq


class AtacStationController(StationController):

    class EnqueuedRides:
        """
        This is a simple class used to model rides that StationController wanted to send on a low bus availability moment.
        These rides will be sent when a bus will become availability to controller.

        """
        def __init__(self, line, station, creation):
            self.line_id = line
            self.station_id = station
            self.creation = creation

        def __str__(self):
            return "Ride to send on line {} from {}".format(self.line_id, self.station_id)

        def __cmp__(self, other):
            """
            Needed by heapq priority queue

            :param other:

            :return:
            """
            return self.creation >= other.creation

    def iteration(self, time_now, expirated_events):
        """
        Simulator calls this function at each iteration. It's the hook to operate.
        In AtacStationController at each iteration different phase will be executed.
        First, it checks if some in-pause-bus is ready to start again after the pause.
        Secondly, it manages arrivals from simulator, and put these bus on in-pause buffer.
        Then, it checks from ATAC log the real rides started in this iteration.
        Finally, it starts simulated rides, according to equeued rides age.

        :param datetime time_now: time of the iteration
        :param list[events] expirated_events: list of expirated events from the last iteration

        :return: nothing
        """
        if self.verbosity > 9: print "atac simulator, iteration".format(time_now)
        bus_before = self.bus_buffer
        sim_departures = 0

        # managing bus in pause
        while self._ready_bus(time_now):
            self._pop_autobus_in_pause()
            self._push_autobus()

        # managing arrivals from simulator
        arrivals = expirated_events
        for arrive in arrivals:
            self._push_autobus_in_pause()
        if self.verbosity > 5 and len(arrivals)>0 : print "Arrived: {}".format(len(arrivals))

        # managing real departures from log
        departures = find_atac_departures(time_now, time_now+self.sampling_rate)
        my_departures = []
        for dep in departures:
            station_id = dep[1]
            instant = dep[2]
            bus_id = dep[3]
            line_id = dep[4]
            is_my_station = self._check_if_my_station(station_id)
            if self.verbosity > 9: print "Real ATAC departure at {} from {}, one of my stations {}, real bus {}, line {}".format(instant,station_id, is_my_station,bus_id, line_id)
            if is_my_station:
                self._push_enqueued(AtacStationController.EnqueuedRides(line_id, station_id, time_now))
                my_departures.append(dep)

        # managing new simulated departures
        while self._have_almost_one_enqueued_ride() and self._have_almost_one_autobus():
            self._pop_autobus()

            sim_departures += 1
            ride = self._pop_enqueued_ride()
            ride.departure_time = time_now

            if self.verbosity > 5 : print "Sending simulated: {}".format(ride)
            self._send_ride_to_simulator(ride)


        if self.verbosity > 9 and len(arrivals)==len(my_departures)==sim_departures==0: print "At this iteration: {} arrivals, {} real departures, {} simulated departures, {} bus before, {} bus after".format(len(arrivals),len(my_departures), sim_departures, bus_before, self.bus_buffer)
        if self.verbosity > 1 and (len(arrivals)!=0 or len(my_departures)!=0 or sim_departures!=0) : print "{} iteration: {} arrivals, {} real departures, {} simulated departures, {} bus before, {} bus after, {} bus in pause, {} enqueued rides".format(time_now, len(arrivals),len(my_departures), sim_departures, bus_before, self.bus_buffer, len(self.in_pause_bus_buffer), len(self.enqueued_rides))
        return

    def _have_almost_one_autobus(self):
        return self.bus_buffer > 0

    def _ready_bus(self, now):
        """
        True if there's almost one ready bus, so pause is finished, False on the other case.

        :param datetime now: expiration bound to compare

        :return: Boolean
        """
        if len(self.in_pause_bus_buffer) != 0:
            if self.in_pause_bus_buffer[0] <= now:
                return True
        return False

    def _pop_autobus(self):
        self.bus_buffer -= 1

    def _pop_autobus_in_pause(self):
        if self.verbosity > 9 : print "bus pause: terminated"
        heapq.heappop(self.in_pause_bus_buffer)

    def _push_autobus(self):
        self.bus_buffer += 1

    def _pop_enqueued_ride(self):
        return heapq.heappop(self.enqueued_rides)

    def _have_almost_one_enqueued_ride(self):
        return len(self.enqueued_rides) > 0

    def _push_enqueued(self, ride):
        heapq.heappush(self.enqueued_rides, ride)

    def _send_ride_to_simulator(self, enqueued):
        self.simulator.send_bus(self.simulator.time_now, enqueued.line_id, enqueued.station_id, 0)

    def _push_autobus_in_pause(self):
        if self.verbosity > 9 : print "bus in pause"
        heapq.heappush(self.in_pause_bus_buffer, self.simulator.time_now + self.pause_duration)

    def __str__(self):
        ret = "Atac Controller at {}".format(self.station_name)
        if self.verbosity > 2:
            for st in self.station_list:
                ret += "\n" + str(st)
        if self.verbosity > 1: ret += "\ntime to wait for bus: {}, bus in buffer: {}, enqueued ride {}".format(self.pause_duration, self.bus_buffer, len(self.enqueued_rides))
        return ret

    def _check_if_my_station(self, station_id):
        return self.stations.get(station_id) != None