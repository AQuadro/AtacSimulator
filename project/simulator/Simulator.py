from project.simulator.AtacStationController import AtacStationController
from project.entity.EventOfArrival import EventOfArrival
from project.entity import dao
from sqlalchemy import func
from datetime import datetime, timedelta
from project.entity.Ride import Ride
from lib.PySettings.SettingsManager import SettingsManager
from project.simulator.RegularityMetricator import RegularityMetricator
import heapq

class Simulator():
    def __init__(self, start_simulation, end_simulation, sampling_rate):
        """
        Init of Simulator.

        :param datetime.datetime start_simulation: expressed in datetime.datetime, start of the simulation
        :param datetime.datetime end_simulation: expressed in datetime.datetime, end of the simulation
        :param datetime.timedelta sampling_rate: expressed in timedelta, gap of time from an interation and the next one.
        """
        self.controllers = []
        self.time_now = start_simulation
        self.events_queue = []
        self.metricators = []
        self.end_simulation = end_simulation
        self.sampling_rate = sampling_rate
        self.verbosity = SettingsManager.getSettings().get_verbosity()

        if self.verbosity>0: print "Setting up simulation from {} to {}, gap {}".format(start_simulation, end_simulation, sampling_rate)

    def _next_sample(self):
        """
        This function update the internal clock of simulation to next step, it's a discrete time events simulation

        :return: nothing
        """
        old = str(self.time_now)
        self.time_now = self.time_now + self.sampling_rate
        if self.verbosity >= 10: print "{} -> {}".format(old, str(self.time_now))

    def loop(self):
        """
        This function starts the simulation loop. It's a blocking call, when it reaches the end, function returns.
        During simulation metricators and controllers are called with function "iteration"

        :return:
        """
        if self.verbosity > 0: print "Starting simulation..."

        while self.time_now < self.end_simulation:
            expirated_events = self._pop_expirated_events()
            time_now = self.time_now
            for controller in self.controllers:
                controller.iteration(time_now, expirated_events)
            for m in self.metricators:
                for e in expirated_events:
                    m.evaluate_ride(e.ride)

            self._next_sample()
        for m in self.metricators:
            m.final_evaluation()
        for m in self.metricators:
            m.print_evaluations()
        if self.verbosity > 0: print "Simulation ended"

    def _add_event(self, ev):
        """
        It adds an event to priority queue of events.

        :param ev:
        :return:
        """
        heapq.heappush(self.events_queue, ev)

    def _exp_elements(self, now):
        """
        True if there's almost one expirated event on the priority queue, False on the other case.

        :param datetime now: expiration bound to compare
        :return: Boolean
        """
        if len(self.events_queue) != 0:
            if self.events_queue[0].expiration <= now:
                return True
        return False

    def _pop_expirated_events(self):
        """
        If an event has an instant minor than now, it has to be removed.
        This function returns a list of expirated EventOfArrival, using time now as compare time.

        :return: List of EventOfArrival
        """
        ex = []
        now = self.time_now
        while self._exp_elements(now):
            # expired events
            ex.append(heapq.heappop(self.events_queue))

        return ex

    def add_controller(self, controller):
        """
        This  function register a controller that will be invoked at every iteration through controller.iteration

        :param controller: A controller, like AtacStationController.

        :return:
        """
        if self.verbosity > 1: print "Adding controller {}".format(str(controller))
        self.controllers.append(controller)

    def add_metricator(self, metricator):
        """
        This  function register a metricator that will be invoked at every iteration through metricator.iteration and at the end of simulation.

        :param metricator: A Metricator, like RegularityMetricator.

        :return:
        """
        if self.verbosity > 1: print "Adding metricator {}".format(str(metricator))
        self.metricators.append(metricator)

    def _find_prev(self, instant, line_id, station_id, limit=5):
        """
        This is a recursive method that returns a real Atac ride that it finds in log.
        The ride is the one on line and station indicated, and it's the nearest possible found ride BEFORE the simulated start time.
        Function is recursive because to find a ride it searches before a start time, and then the arrival log.
        Sometimes, due to gps problems or OutOfService of the autobus, a departure is immediately found, but arrival is missing, so
        the function calls itself to find a previous departure time.

        :param datetime instant: The time when simulated ride is departed
        :param Integer line_id: Id of the involved line.
        :param Integer station_id: Id of the involved station.
        :param Integer limit: number of admitted fails on searching a arrival time. Default is 5. (Tipically if the function
        reaches the limit, saved logs are not enough.

        :return: (datetime, timedelta) (departure instant, ride duration)
        """
        limit = limit - 1

        prev_departure = dao.find_prev_filtered_departure(instant, line_id, station_id)
        prev_departure_instant = datetime.strptime(prev_departure[2], '%Y-%m-%d %H:%M:%S')

        prev_arrival = dao.find_prev_filtered_arrival(prev_departure_instant, line_id, station_id, prev_departure[3])

        if limit == 0:
            print "next_departure : {}".format(prev_departure_instant)
            return None,None

        if prev_arrival is not None:
            prev_arrival_instant =  datetime.strptime(prev_arrival[2], '%Y-%m-%d %H:%M:%S')
            print "prev_departure : {}, prev_arrival {}".format(prev_departure_instant, prev_arrival_instant)
            return prev_departure_instant, (prev_arrival_instant - prev_departure_instant)
        else:
            return self._find_prev(prev_departure_instant, line_id, station_id, limit)

    def _find_next(self, instant, line_id, station_id, limit):
        """
        This is a recursive method that returns a real Atac ride that it finds in log.
        The ride is the one on line and station indicated, and it's the nearest possible found ride AFTER the simulated start time.
        Function is recursive because to find a ride it searches before a start time, and then the arrival log.
        Sometimes, due to gps problems or OutOfService of the autobus, a departure is immediately found, but arrival is missing, so
        the function calls itself to find a next departure time.

        :param datetime instant: The time when simulated ride is departed
        :param Integer line_id: Id of the involved line.
        :param Integer station_id: Id of the involved station.
        :param Integer limit: number of admitted fails on searching a arrival time. Default is 5. (Tipically if the function
        reaches the limit, saved logs are not enough.

        :return: (datetime, timedelta) (departure instant, ride duration)
        """
        limit = limit - 1

        next_departure = dao.find_next_filtered_departure(instant, line_id, station_id)
        next_departure_instant = datetime.strptime(next_departure[2], '%Y-%m-%d %H:%M:%S')
        next_arrival = dao.find_next_filtered_arrival(next_departure_instant, line_id, station_id, next_departure[3])

        if limit == 0:
            print "next_departure : {}".format(next_departure_instant)
            return None,None


        if next_arrival is not None:
            next_arrival_instant =  datetime.strptime(next_arrival[2], '%Y-%m-%d %H:%M:%S')
            print "next_departure : {}, next_arrival {}".format(next_departure_instant, next_arrival_instant)
            return next_departure_instant, (next_arrival_instant - next_departure_instant)
        else:
            return self._find_next(next_departure_instant, line_id, station_id, limit)

    def send_bus(self, instant, line_id, station_id, bus_id):
        """
        Controller sends request to send a bus at 'instant' time. This function triggers the start of a simulated bus.

        :param datetime instant: time of departure
        :param Integer line_id: id of line involved
        :param Integer station_id: id of the involved station
        :param Integer bus_id: id of the involved bus

        :return: Ride object representing the simulated ride
        """
        prev_departure, prev_duration = self._find_prev(instant, line_id, station_id, 5)
        next_departure, next_duration = self._find_next(instant, line_id, station_id, 5)

        print "prev_duration : {} , next_duration : {}".format(prev_duration,next_duration)

        end = self._interpolate(instant, prev_departure, prev_duration, next_departure, next_duration)
        ride = Ride(line_id=str(line_id),autobus_id=bus_id,departure_time=instant,arrival_time=end)
        self._add_event(EventOfArrival(ride_id=ride.id, creation=instant, expiration=end, ride=ride))
        return ride

    def eta(self, ride):
        """
        This method calculates an estimation of the arrival time of a simulated ride, using previous logged rides.
        This data are not the real duration, because controllers cannot have access to future data.

        :param Ride ride: ride on which estimate an eta

        :return: datetime estimated time of arrival
        """
        prev, duration_prev = self._find_prev(ride.departure_time, ride.line_id, ride.station_id)
        return ride.departure_time + duration_prev

    def _interpolate(self, instant, prev_dep, prev_duration, next_dep, next_duration):
        """
        This function is used to interpolate duration of prev and next found rides. Simulated ride will be long as the interpolation of these two duration.

        :param datetime instant: instant of simulated ride departure
        :param datetime prev_dep: instant of prev real ride departure
        :param timedelta prev_duration: duration of prev real ride
        :param datetime next_dep: instant of next real ride departure
        :param timedelta next_duration: duration of next real ride

        :return: timedelta interpolated duration
        """
        t_x1 =  (instant - prev_dep).total_seconds()
        x2_t =  (next_dep - instant).total_seconds()
        x2_x1 = (next_dep - prev_dep).total_seconds()

        res = (t_x1*prev_duration.total_seconds())/(x2_x1) + (x2_t*next_duration.total_seconds())/x2_x1
        return instant + timedelta(seconds=res)


def start():
    """
    This static function configures and starts the simulation according to saved settings.

    :return:
    """
    begin_simulation = SettingsManager.getSettings().get_begin_simulation()
    end_simulation = SettingsManager.getSettings().get_end_simulation()
    sampling_rate_seconds = SettingsManager.getSettings().get_sampling_rate()
    station_name = SettingsManager.getSettings().get_station_name()
    sampling_rate = timedelta(seconds=sampling_rate_seconds)
    pause_duration = timedelta(seconds=SettingsManager.getSettings().get_pause_duration())
    bus_buffer = SettingsManager.getSettings().get_bus_buffer()

    simulator = Simulator(begin_simulation, end_simulation, sampling_rate)
    simulator.add_controller(AtacStationController(sampling_rate, station_name, pause_duration, simulator, bus_buffer))
    simulator.add_metricator(RegularityMetricator())

    simulator.loop()
