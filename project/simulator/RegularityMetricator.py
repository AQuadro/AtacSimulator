from project.entity.Ride import Ride
from datetime import timedelta
from Metricator import Metricator


class RegularityMetricator(Metricator):
    """
    The scope of this Metricator is to evaluate the Regularity of the rides.
    """

    def __init__(self):
        Metricator.__init__(self)
        self.prev_departure_times = {}
        self.sums = {}
        self.means = {}
        self.logs = {}
        self.totals = {}
        self.std = {}

    def evaluate_ride(self, ride):
        """
        This method is used to notify to Metricator the arrival of a Ride to evaluate.

        :param Ride ride:

        :return:
        """
        line_id = ride.line_id

        if self._is_first(line_id):
            self.prev_departure_times[line_id] = ride.departure_time
            self.sums[line_id] = timedelta(seconds=0)
            self.means[line_id] = timedelta(seconds=0)
            self.logs[line_id] = []
            self.totals[line_id] = 0.0
            return

        prev = self.prev_departure_times.get(line_id)
        next = ride.departure_time
        diff = next - prev
        self.logs[line_id].append(ride)
        self.sums[line_id] = self.sums[line_id] + diff
        self.totals[line_id] = self.totals[line_id] + 1

    def _is_first(self, line_id):
        return self.prev_departure_times.get(line_id) is None

    def final_evaluation(self):
        """
        This method is called at the end of simulation.

        :return:
        """
        import math
        for e in self.sums:
            tmp = self.sums[e].total_seconds()/self.totals[e]
            self.means[e] = timedelta(seconds=tmp)
        for log_line in self.logs:
            std = 0.0
            for log in self.logs[log_line]:
                dv = log.arrival_time - log.departure_time - self.means[log_line]

                std += pow(int(dv.total_seconds()), 2)/len(self.logs[log_line])
            self.std[log_line] = math.sqrt(std)

    def print_evaluations(self):
        for e in self.means:
            print "Means for line {} : {}, std: {} m".format(e,self.means[e], self.std[e]/60)
