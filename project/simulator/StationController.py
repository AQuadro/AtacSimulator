from project.entity.dao import *
from lib.PySettings.SettingsManager import SettingsManager


class StationController:

    def __init__(self, sampling_rate, station_name, pause_duration, simulator, bus_buffer):
        """
        Constructor of a generic Station Controller.

        :param timedelta sampling_rate: duration of each iteration of the simulation
        :param string station_name: symbolic name of the station, like Anagnina
        :param timedelta pause_duration: pause duration from a bus arrival to the next departure of that bus
        :param Simulator simulator: Simulator instance
        :param Integer bus_buffer: size of available bus at beginning of simulation
        """
        self.verbosity = SettingsManager.getSettings().get_verbosity()
        self.sampling_rate = sampling_rate
        self.station_name = station_name
        self.station_list = find_stations_by_name(station_name)
        self.enqueued_rides = [] # if i cannot send a bus, the ride will be in waiting of a bus
        self.bus_buffer = bus_buffer
        self.in_pause_bus_buffer = []
        self.pause_duration = pause_duration
        self.simulator = simulator

        if self.verbosity > 0: "Creating " + str(self)

        self.stations = {}
        for st in self.station_list:
            self.stations[st.id] = st

    def iteration(self, time_now, expirated_events):
        """
        Simulator calls this function at each iteration. It's the hook to operate.

        :param datetime time_now: time of the iteration
        :param list[events] expirated_events: list of expirated events from the last iteration

        :return: nothing
        """
        pass

    def __str__(self):
        ret = "Controller at {}".format(self.station_name)
        if self.verbosity > 2:
            for st in self.station_list:
                ret += "\n" + str(st)
        if self.verbosity > 1: ret += "\ntime to wait for bus: {}, bus in buffer: {}, enqueued ride {}".format(self.pause_duration, self.bus_buffer, len(self.enqueued_rides))
        return ret