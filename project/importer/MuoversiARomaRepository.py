import requests
from lib.PySettings import SettingsManager
import json
from project.entity.dao import *
import random


def post_for_stops_by_name(station_name):
    return json.loads(_post_muoversiaroma(_build_smart_research_request(station_name)).content)["result"]["paline_extra"]


def post_paths_by_line(line_name):
    return json.loads(_post_muoversiaroma(_build_smart_research_request(line_name)).content)["result"]["percorsi"]


def post_stops_by_path(line_name):
    return json.loads(_post_muoversiaroma(_build_path_stop_request(line_name)).content)["result"]["fermate"]



def _post_muoversiaroma(request_body):
    settings = SettingsManager.getSettings()
    error = True

    while error:
        response = requests.post(settings.get_base_url_muoversiaroma(), json=request_body)
        error = not response.ok

        if error:
            print response.content

    return response


def _build_smart_research_request(station_name):
    return _build_request("paline_smart_search", [station_name])


def _build_path_stop_request(path_id):
    return _build_request("paline_percorso_fermate", [path_id, ""])


def _build_autobus_geo_request(station_name):
    return _build_request("mappa_layer", [["posizione_bus", station_name]])


def _build_request(method, param):
    obj = dict()
    obj["jsonrpc"] = "2.0"
    obj["id"] = "ID" + str(random.randint(1,100))

    obj["method"] = method
    param.append("it")
    obj["params"] = param

    return obj
