import requests
from Queue import Queue
from threading import Thread

import sys

from lib.PySettings import SettingsManager
import json
from project.entity.dao import *
from MuoversiARomaRepository import *
from functools import wraps
from sqlalchemy import func

def search_path(path_id):
    return post_stops_by_path(path_id)


def find_inverted_path(path, list):
    type_to_find = ""
    dev = False
    if path["carteggio"] == "A":
        type_to_find = "R"
        dev = False
    elif path["carteggio"] == "R":
        type_to_find = "A"
        dev = False
    elif path["carteggio"] == "RD":
        type_to_find = "AD"
        dev = True
    elif path["carteggio"] == "AD":
        type_to_find = "RD"
        dev = True

    for inver_path_index in range(len(list)):
        if list[inver_path_index]["carteggio"] == type_to_find:
            return list.pop(inver_path_index), dev
    print "companion not found"
    return None, False


def search_path_by_line(line_id, head_id):
    print "scanning line:" + str(line_id)
    paths_obj = post_paths_by_line(line_id)
    lines = []

    if len(paths_obj) > 2:
        print "piu di 2 percorsi per una linea!!! " + str(line_id) + "  " + str(len(paths_obj))

    while len(paths_obj) > 0:
        path_obj = paths_obj.pop()
        path_obj_inverted, dev = find_inverted_path(path_obj, paths_obj)

        id = path_obj["id_percorso"]
        type_trip = path_obj["carteggio"]
        id_line = path_obj["id_linea"]
        direction = path_obj["direzione"]

        stops = search_path(id)
        departure_stop_id = stops[0]["id_palina"]
        arrival_stop_id = stops[-1]["id_palina"]

        if id_line != line_id:
            print "Problema sulla linea, non coincidono " + str(line_id) + " su " + str(id_line)
            continue

        if dev:
            id_line = id_line + "D"

        if departure_stop_id == head_id:
            if departure_stop_id != arrival_stop_id:
                dep_stop_id = departure_stop_id
                middle_stop_id = arrival_stop_id
                departure_line_id = id
                if path_obj_inverted == None:
                    print "linea non associata a nessun ritorno, ignorata:" + str(id_line) + " su " + str(id)
                    continue
                inverted_id = path_obj_inverted["id_percorso"]
                arrival_line_id = inverted_id
                loop = 0

            else:
                dep_stop_id = departure_stop_id
                middle_stop_id = arrival_stop_id
                departure_line_id = id
                arrival_line_id = id
                loop = 1
                print "percorso loop su: " + str(id_line) + " su " + str(id)
            print "added " + id_line
            lines.append(Line(departure_station_id=dep_stop_id, middle_station_id=middle_stop_id, id=id_line, departure_line_id=departure_line_id,arrival_line_id=arrival_line_id, loop_line=loop))
        elif arrival_stop_id == head_id:
            middle_stop_id = departure_stop_id
            dep_stop_id = arrival_stop_id
            arrival_line_id = id
            if path_obj_inverted == None:
                print "linea non associata a nessun andata, ignorata:" + str(line_id) + " su " + str(id)
                continue
            inverted_id = path_obj_inverted["id_percorso"]
            departure_line_id = inverted_id
            loop = 0
            print "added " + id_line
            lines.append(Line(departure_station_id=dep_stop_id, middle_station_id=middle_stop_id, id=id_line, departure_line_id=departure_line_id,arrival_line_id=arrival_line_id, loop_line=loop))
        elif departure_stop_id != head_id and arrival_stop_id != head_id:
            print "Un percorso ha come capolinea due stazioni diverse dalla stazione ricercata, forse transito: L" + str(line_id) + " su " + str(id)

        else:
            print "errore sconosciuto su " + str(line_id) + " su " + str(id)

    return lines


def search_stops_by_settings():
    station_name = SettingsManager.getSettings().get_station_name()
    stops_list = post_for_stops_by_name(station_name)

    stations = []
    lines = []

    for obj in stops_list:
        id_station = obj["id_palina"]
        stations.append(Station(id=id_station, direction=obj["direzioni"], name=station_name, monitor=1))

        this_lines = obj["linee_info"]
        for line_obj in this_lines:
            lines.extend(search_path_by_line(line_obj["id_linea"], head_id=id_station))

        this_lines = obj["linee_extra"]
        for line_obj in this_lines:
            if len(line_obj) > 0:
                lines.extend(search_path_by_line(line_obj, head_id=id_station))

    populate(stations=stations, lines=lines)


def save_position_all_net():
    thread_number = SettingsManager.getSettings().get_threads_number()
    lines = find_all_lines()
    queue = Queue()
    for line in lines:
        queue.put(line)

    for _ in range(thread_number):
        t = Thread(target=thread_line_logger, args=[queue])
        t.daemon = True
        t.start()
    try:
        queue.join()
    except (KeyboardInterrupt, SystemExit):
        sys.exit()
    print "All done, bye!"


def save_bus_position_on_line(line):
    print "saving bus position on line: " + str(line.id)
    if line.departure_line_id != None:
        save_position_on_path(search_path(line.departure_line_id),line.id)
    if line.arrival_line_id != None:
        save_position_on_path(search_path(line.arrival_line_id),line.id)


def save_position_on_path(path,line_id):
    dep = path[0]
    arr = path[-1]
    save_position_on_station(dep,line_id)
    save_position_on_station(arr,line_id)


def save_position_on_station(station,line_id):
    try:
        bus = station["veicolo"]["id_veicolo"]
        station = station["id_palina"]
        populate(log_events=[LogEvent(station_id=int(station),instant=func.now(),bus_id=int(bus),line_id=line_id)])
        print "saved bus " + str(bus) + ", station " + str(station) + ", line " + str(line_id)
    except Exception:
        pass
    return


def thread_line_logger(queue):
    while not queue.empty():
        line = queue.get()
        save_bus_position_on_line(line)
        queue.task_done()


def daemon_logger():
    import time
    interval_seconds = SettingsManager.getSettings().get_interval_second_log()

    while True:
        print "iteration"
        try:
            save_position_all_net()
            time.sleep(interval_seconds)
        except (KeyboardInterrupt, SystemExit):
            print "exiting"


##
##
##
##
